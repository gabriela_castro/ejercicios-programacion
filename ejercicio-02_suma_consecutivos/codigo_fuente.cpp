#include <stdio.h>

//Ejercicio 02: Suma de N�meros Consecutivos
//C�digo fuente en C

int main (){
    int numero;
    int suma=0;
    
    printf("Ingresa un n�mero entero entre 1 y 50 para obtener la suma de los numeros consecutivos desde el 1 hasta tu n�mero ingresado\n");
    scanf("%d", &numero);
    
    suma=((numero*(numero+1))/2);
    
    printf("\nLa suma de los numeros consecutivos desde el 1 hasta tu numero es: %d\n", suma);
}
