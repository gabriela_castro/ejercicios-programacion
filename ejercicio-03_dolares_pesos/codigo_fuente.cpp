#include<stdio.h>

//Ejercicio 03: Conversi�n de Divisas
//C�digo fuente en C

int main() {
    int opcion;
    float valor_dolar, cantidad_dolar, cantidad_pesos, cambio;
    char valor_conocido;
    char convertir='c';
    
    while (convertir!='e'){
        printf ("Presiona 1 para convertir de dolares a pesos.\nPresiona 2 para convertir de pesos a dolares");
        scanf("%d", &opcion);
        
            if (opcion==1){
                printf("\n�Conoces el valor del tipo de cambio del dolar el dia de hoy? \nPresiona s para SI, presiona n para NO");
                scanf("%s", &valor_conocido);
                
                    if (valor_conocido=='s'){
                        printf("\nIngresa el valor del tipo de cambio del dolar\n");
                        scanf("%f", &valor_dolar);
                        printf("\nIngresa la cantidad de dolares a convertir\n");
                        scanf("%f", &cantidad_dolar);
                        cambio=valor_dolar*cantidad_dolar;
						printf("La cantidad ingresasa en dolares equivalen en pesos a \n$ %.2f", cambio);
                    }
                    
                    else if (valor_conocido=='n'){
                        printf ("\nIngresa la cantidad de dolares a convertir. El tipo de cambio estimado sera de $20mxn por cada dolar");
                        scanf("%f", &cantidad_dolar);
                        cambio=20*cantidad_dolar;
						printf("La cantidad ingresasa en dolares, con un tipo de cambio estimado en $20mxn, equivalen en pesos a \n$ %.2f", cambio);
                    }
            }
            
            else if (opcion==2){
                printf("\n�Conoces el valor del tipo de cambio del dolar el dia de hoy? Presiona s para s�, presiona n para no");
                scanf("%s", &valor_conocido);
                
                if (valor_conocido=='s'){
                        printf("\nIngresa el valor del tipo de cambio del dolar");
                        scanf("%f", &valor_dolar);
                        printf("\nIngresa la cantidad de pesos a convertir");
                        scanf("%f", &cantidad_pesos);
                        cambio=cantidad_pesos/valor_dolar;
						printf("La cantidad ingresasa en pesos equivalen en dolares a \n$ %.2f", cambio);
                    }
                    
                    else if (valor_conocido=='n'){
                        printf ("\nIngresa la cantidad de pesos a convertir. El tipo de cambio estimado sera de $0.05dllrs por cada peso");
                        scanf("%f", &cantidad_pesos);
                        cambio=0.05*cantidad_pesos;
						printf("La cantidad ingresasa en pesos, con un tipo de cambio estimado en $0.05dllrs, equivalen en dolares a \n$ %.2f", cambio);
                    }
            }
            
        printf("\nPresiona c para convertir otra cantidad. Presiona e para salir");
        scanf("%s", &convertir);
    }
    
    return 0;
}

