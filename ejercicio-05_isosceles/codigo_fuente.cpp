#include <stdio.h>

//Ejercicio 05: Calculo del Perimetro de un Triangulo Isosceles
//Codigo Fuente en C

int main (){
	float base, lado, perimetro;
	char calcular='c';

	while(calcular!='e'){
		printf("Para calcular el perimetro de un triangulo isosceles necesitas la medida de su base y de uno de sus dos lados. Ingresa los datos requeridos para calcularlo.\n\n Por favor, ingresa la medida de la base");
		scanf("%f", &base);
		printf("Por favor, ingresa la medida de cualquiera de los dos lados restantes");
		scanf("%f", &lado);
		perimetro=(lado*2)+base;
		printf("\nEl perimetro del triangulo isosceles con las medidas ingresadas es de: %.2f", perimetro);
		
		printf("\n\n Presiona c para calcular el perimetro de otro triangulo isosceles. Presiona e para salir");
	    scanf("%s", &calcular);
		printf("\n\n");		
	}
	
	return 0;
}
