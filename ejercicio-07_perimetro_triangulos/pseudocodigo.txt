Pseudocodigo: Ejercicio 07

flotante FUNCION equilatero(flotante lado)
	INICIO
		devuelve=lado*3
	FIN
flotante FUNCION isosceles(flotante lado, flotante base)
	INICIO	
		devuelve=(lado*2)+base
	FIN
flotante FUNCION escaleno(flotante lado_chiquito, flotante lado_grande, flotante base)
	INICIO
		devuelve=lado_chiquito+lado_grande+base
	FIN

INICIO
	Numerico Entero opcion
	Numerico Flotante base
	Numerico Flotante lado
	Numerico Flotante lado_chiquito
	Numerico Flotante lado_grande
	Numerico Flotante perimetro
	Caracter calcular = 'c'

	While (calcular!='e') hacer
		Leer "Presiona 1 para calcular el perimetro de un triangulo equilatero. Presiona 2 para calcular el perimetro de un triangulo isosceles. Presiona 3 para calcular el perimetro de un triangulo escaleno", opcion 
	
			Si (opcion==1) entonces
				Leer "El triangulo equilatero tiene todos los lados iguales. Por favor, ingresa la medida de cualquier lado para calcular el perimetro", lado	
				perimetro=equilatero(lado)
				Escribir "El perimetro del triangulo equilatero con la medida ingresada por lado es de: ", perimetro
				FIN Si

			Si (opcion==2) entonces
				Leer "Por favor, ingresa la medida de la base", base
				Leer "Por favor, ingresa la medida de cualquiera de los dos lados restantes", lado
				perimetro=isosceles(lado, base)
				Escribir "El perimetro del triangulo isosceles con las medidas ingresadas es de: ", perimetro
				FIN Si

			Si (opcion==3) entonces
				Leer "Por favor, ingresa la medida del cateto adyacente a la hipotenusa (la base)", base
				Leer "Por favor, ingresa la medida del cateto opuesto a la hipotenusa (el lado mas chiquito)", lado_chiquito
				Leer "Por favor, ingresa la medida de la hipotenusa (el lado mas grande)", lado_grande
				perimetro=escaleno(lado_chiquito, lado_grande, base)
				Escribir "El perimetro del triangulo escaleno con las medidas ingresadas es de: ", perimetro
				FIN Si
	
		Leer "Presiona c para calcular el perimetro de otro triangulo. Presiona e para salir", calcular
	FIN While
FIN