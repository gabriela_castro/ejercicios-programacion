#include <stdio.h>

//Ejercicio 08: Tablas de Multiplicar
//Codigo Fuente en C

int main () {
	int numero, multiplo, i;
	char multiplica='m';

	i=0;

	while (multiplica!='e'){
		printf("Ingresa el numero del que desees obtener la tabla de multiplicar. Los multiplos mostrados iran del multiplo 2 al 10\n");
		scanf("%d", &numero);
		
		printf("\nLa tabla de multiplicar, del 2 al 10, del numero ingresado es: \n");
		
		for(i=0; i<=10; i++){
			if (i>=2 && i<=10){
				multiplo=i*numero;
				printf("%d %s", multiplo, " ");
			}
		}
	
		printf("\n\nPresiona m para obtener la tabla de multiplicar de otro digito. Presiona e para salir.");
		scanf("%s", multiplica);
		printf("\n\n");
	}
	
	return 0;
}
