#include <stdio.h>

//Ejercicio 09: Desigualdad de un Triangulo
//Codigo Fuente en C

int main (){
	float lado_a, lado_b, lado_c;
	char comprobar='c';

	while (comprobar!='e'){
		printf("Para comprobar si se puede formar un triangulo con tres medidas que poseas, ingresa los siguientes datos:\n");
		printf("\nIngresa la medida del primer lado \n");
		scanf("%f", &lado_a);
		printf("\nIngresa la medida del segundo lado \n");
		scanf("%f", &lado_b);
		printf("\nIngresa la medida del tercer lado \n");
		scanf("%f", &lado_c);

			if (lado_a+lado_b>lado_c && lado_a+lado_c>lado_b && lado_b+lado_c>lado_a){
				printf("\nCon las medidas de los tres segmentos ingresados SI es posible formar un triangulo.");
			}
			else {
			printf ("\nLas medidas ingresadas no cumplen con la regla de la Desigualdad del Triangulo.\nCada segmento identificado como a, b y c debe cumplir con las tres relaciones siguientes:\na+b>c , a+c>b , b+c>a");
			}
	printf("\nPresiona c para comprobar tres medidas para formar un triangulo. Presiona e para salir\n");
	scanf("%s", comprobar);
	printf("\n\n");
	}
	
	return 0;
}
