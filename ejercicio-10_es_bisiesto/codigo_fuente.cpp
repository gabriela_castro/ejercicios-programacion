#include <stdio.h>

//Ejercicio 10: Lap Year
//Codigo Fuente en C

int main (){
	int year;
	char comprobar='c';

	while (comprobar!='e'){
		printf("Se puede saber si un anio sera bisiesto si la fecha es divisible entre 4 y 400, resultando en un residuo igual a 0, pero no entre 100.\n");
		printf("\nPor favor, ingresa el anio que desees verificar\n");
		scanf("%d", &year);
		
			
			if (year%4==0 && year%100!=0){
				printf("\nEl anio ingresado, %d %s", year, ", SI es bisisesto.");
			}
			else {
				printf("\nEl anio ingresado, %d %s", year, ", NO es bisiesto.");
			}

		printf("\n\nPresiona c para comprobar, de nuevo, si otro anio sera bisiesto. Presiona e para salir");
		scanf("%s", &comprobar);
		printf("\n\n");
	}
	return 0;
}
