#include <stdio.h>

//Ejercicio 11: Numero del Mes
//Codigo Fuente en C

int main(){
	int mes;
	char comprobar='c';

	while (comprobar!='e'){
		printf("Por favor, ingresa el numero del mes cuyo nombre desees saber. \nNOTA: Toma en cuenta que Enero es el mes 0 y Diciembre el mes 11.\n");
		scanf("%d", &mes);

		if (mes>=0 && mes<=11){
			switch (mes){
				case 0:
					printf("\nEl mes 0 corresponde a Enero");
					break;
				case 1:
					printf("\nEl mes 1 corresponde a Febrero");
					break;
				case 2:
					printf("\nEl mes 2 corresponde a Marzo");
					break;
				case 3:
					printf("\nEl mes 3 corresponde a Abril");
					break;
				case 4:
					printf("\nEl mes 4 corresponde a Mayo");
					break;
				case 5:
					printf("\nEl mes 5 corresponde a Junio");
					break;
				case 6:
					printf("\nEl mes 6 corresponde a Julio");
					break;
				case 7:
					printf("\nEl mes 7 corresponde a Agosto");
					break;
				case 8:
					printf("\nEl mes 8 corresponde a Septiembre");
					break;
				case 9:
					printf("\nEl mes 9 corresponde a Octubre");
					break;
				case 10:
					printf("\nEl mes 10 corresponde a Noviembre");
					break;
				case 11:
					printf("\nEl mes 11 corresponde a Diciembre");
					break;
			}
		}
		
		if (mes>11){
			printf("\n El numero ingresado no corresponde a ningun mes.");
		}
		
		printf("\n\n Presiona c para ingresar otro numero y conocer el mes asociado. Presiona e para salir\n");
		scanf("%s", comprobar);
		printf("\n\n");
	}
}
