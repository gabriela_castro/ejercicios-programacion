#include <stdio.h>

//Ejercicio 13: Ping-Pong
//Codigo Fuente en C

int main(){
	int i=1;
	
	for (i==1; i<=100; i++){
		
		if(i%3!=0 && i%5!=0){
			printf("%d %s", i, " ");
		}

		if(i%3==0 && i%5!=0){
			printf(" PING ");
		}
		if(i%5==0 && i%3!=0){
			printf(" PONG ");
		}
		
		if (i%3==0 && i%5==0){
			printf(" PING-PONG ");
		}

	}
	return 0;
}